<?php

namespace SpaceTradersCli;

use Exception;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class Login extends Command
{
  protected static $defaultName = 'login';
  protected static $defaultDescription = 'Login with a token';

  // @var InputInterface
  protected $input;

  /** @var MessageBusInterface */
  private $bus;

  protected function configure()
  {
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $helper = $this->getHelper('question');
    $question = new Question('Enter the token to login in: ');
    $token = $helper->ask($input, $output, $question);

    $tokenHandler = new TokenHandler();
    $tokenHandler->set($token);

    $output->writeln("Token Stored: ".$tokenHandler->get());

    return 0;
  }
}
