<?php

namespace SpaceTradersCli;

use Exception;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class TokenHandler
{
  var $token;

  var $cache;

  public function __construct()
  {
    $this->cache = new FilesystemAdapter('spacetraders', 0, getcwd().'cache/');
  }

  public function set(string $token): void
  {
    $this->setToken($token);
  }

  public function setToken(string $token): void
  {
    $tokenCache = $this->cache->getItem('token');
    $tokenCache->set($token);
    $this->cache->save($tokenCache);
  }

  public function get(): string
  {
    return $this->getToken();
  }

  public function getToken(): string
  {
    $tokenCache = $this->cache->getItem('token');
    if (!$tokenCache->isHit()) {
      throw new Exception("You need to log in, use the login command");
    }
    return $tokenCache->get();
  }
}
