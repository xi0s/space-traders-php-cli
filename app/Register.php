<?php

namespace SpaceTradersCli;

use Exception;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class Register extends Command
{
  protected static $defaultName = 'register';
  protected static $defaultDescription = 'Regsiter a new agent';

  protected function configure()
  {
    $this
      ->addArgument('callsign', InputArgument::REQUIRED, 'Your username/callsign')
      ->addOption('endpoint', 'e', InputOption::VALUE_REQUIRED, 'Endpoint for the space traders API', "https://api.spacetraders.io/v2/");
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $endpoint = $input->getOption('endpoint');

    $factions = $this->getFactions($endpoint);

    $helper = $this->getHelper('question');
    $factionSelector = new ChoiceQuestion('Select a faction', $factions);
    $faction = $helper->ask($input, $output, $factionSelector);

    $client = new Client(['headers' => ['Content-Type' => 'application/json']]);
    $registerRequest = $client->request('POST', $endpoint.'register', [
      'json' => [
        'symbol' => $input->getArgument('callsign'),
        'faction' => $faction,
      ],
    ]);

    $response = json_decode($registerRequest->getBody());
    $token = $response->data->token;

    $output->writeln("Token: ".$token);

    $tokenHandler = new TokenHandler();
    $tokenHandler->setToken($token);

    return 0;
  }

  private function getFactions(string $endpoint)
  {

    $client = new Client(['headers' => ['Content-Type' => 'application/json']]);
    $factionsResponse = $client->request('GET', $endpoint.'factions', ['query' => ['limit' => 20]]);
    if($factionsResponse->getStatusCode() != 200){
      throw new Exception("Factions could not be determined", $factionsResponse->getStatusCode());
    }
    $factionsData = json_decode($factionsResponse->getBody());
    $factions = [];
    foreach($factionsData->data as $faction){
      $factions[$faction->symbol] = $faction->name;
    }
    if($factionsData->meta->total > $factionsData->meta->limit){
      // Throw warning some factions not there
    }
    return $factions;
  }
}
